# json-format-tree-view
pure JavaScript JSON formatter / viewer which helps render JSON objects just like a collapsible tree view. visualise json using "showJSON()" method, which accepts 3 arguments - json file, optional: visualise to max level (-1 unlimited, 0..n), optional: collapse all at level (-1 unlimited, 0..n).

[Live Demo](https://example-javascript.herokuapp.com/json)
